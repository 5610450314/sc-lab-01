package Controll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import View.SoftwareFrame;
import Model.Card;

public class CardTest {
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new CardTest();
	}

	public CardTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		//frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		frame.setBounds(500, 500, 400, 400);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card card1 = new Card();
		card1.add(500);
		frame.setResult(card1.toString());
		card1.pay(250);
		frame.extendResult(card1.toString());
		card1.add(1000);
		frame.extendResult(card1.toString());
		card1.pay(350);
		frame.extendResult(card1.toString());
		card1.getBalance();
		frame.extendResult(card1.toString());
		
		
		//frame.setResult(card1.toString());
		//frame.extendResult("\t" + stu2.toString());
		//frame.extendResult(stu2.toString());

	}

	ActionListener list;
	SoftwareFrame frame;

}
